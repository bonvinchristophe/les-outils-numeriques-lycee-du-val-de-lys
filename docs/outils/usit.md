---
author: Christophe Bonvin
title:  🔨 Créer, gérer des plans de classe, Ubisit
tags:
    - niveau débutant 👍
    - inscription ✖️
    - version gratuite
---
:hammer: **Ubisit : Créer, gérer des plans de classe** :hammer:
 

**Ubisit** est une application libre et gratuite de la plateforme **Forge des communs numériques éducatifs** créée par Soren Starck. Il permet de créer un très rapidement, et sans installation, un plan de classe, de tirer au sort des élèves.

Il est possible de faire ses "screens" à l'avance, de les enregistrer sur un support (clé USB, espace de travail) et de les ouvrir par la suite.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire/](https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Ubisit](https://tube-numerique-educatif.apps.education.fr/w/3nzhE61Rn1TaX9W1vGxv6T){:target="_blank" }
    
