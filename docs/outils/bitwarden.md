---
author: Christophe Bonvin
title:  🔨  Coffre fort numérique, gérer ses mots de passe, Bitwarden 
tags:
    - niveau débutant 👍
    - inscription ✔️
    - version gratuite
---
:hammer: **Bitwarden : Coffre fort numérique** :hammer:
 

**Bitwarden** est un logiciel de sauvegarde de mots de passe (coffre-fort numérique). Lors de l’installation de ce logiciel, il faut définir un mot de passe maître qui sera le seul à retenir. Ensuite vous pouvez y enregistrer tous vos mots de passe qui seront stockés dans un fichier crypté. (Théoriquement il faut un mot de passe fort et unique pour chaque site qui demande une authentification). Ce logiciel peut également permettre de **générer des mots de passe forts** et de les enregistrer automatiquement. 

Vous pouvez l’installer sur vos différents périphériques (tablettes, smartphones et ordinateurs) et ainsi avoir accès à tous vos mots de passe synchronisés sur vos appareils. 

!!! note "Prise en main"
    - Lien vers l’outil : [https://bitwarden.com/](https://bitwarden.com/){:target="_blank" }
    - Lien vers un tutoriel : [Installer et utiliser Bitwarden](https://lecrabeinfo.net/utiliser-bitwarden-gestionnaire-mots-de-passe-gratuit-et-open-source.html){:target="_blank" }

!!! info "Remarque"
    Une inscription est nécessaire. Vous pouvez avoir accès à vos mots de passe sur votre mobile ou sur n'importe quel appareil en vous connectant au site Bitwarden.
    Une version payante est disponible mais la version gratuite est largement suffisante.

!!! danger "Conseil sur la sécurité"
    Le mot de passe que vous définirez pour accéder à Bitwarden doit être fort pour que cette solution soit efficace. Il faudra veiller à ne pas l'oublier car il est difficile de le récupérer.


