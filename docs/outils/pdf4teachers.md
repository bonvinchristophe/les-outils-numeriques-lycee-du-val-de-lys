---
author: Christophe Bonvin
title:  🔨  Annoter et corriger des exercices PDF, PDF4Teachers
tags:
    - niveau intermédiaire 👌
    - version gratuite
    - inscription ✖️
---
:hammer: **PDF4Teachers : Annoter et corriger des exercices PDF** :hammer:
 

C’est un logiciel présent sur Linux, Mac et Windows, qui permet de convertir les fichiers image envoyés par les élèves en PDF et de les annoter. Il garde en mémoire le barème et les annotations pour les appliquer plus rapidement. Il calcule automatiquement la note

!!! info "Remarque"
    Une version payante est disponible mais la version gratuite est largement suffisante.


!!! note "Prise en main"
    - Lien vers l’outil : [https://pdf4teachers.org/](https://pdf4teachers.org/){:target="_blank" }
    - Lien vers un tutoriel :  [Utiliser Pdf4Teachers](https://ladigitale.dev/digiplay/#/v/63541d6f53e35){:target="_blank" }
    - [Documentation](https://pdf4teachers.org/Documentation/){:target="_blank" } de l’éditeur avec les différentes fonctionnalités

