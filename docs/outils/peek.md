---
author: Christophe Bonvin
title:  🔨  Enregistrer votre écran sous Windows
tags:
    - niveau intermédiaire 👌
    - version gratuite
    - inscription ✖️
---
:hammer: **Peek Screen Recorder : Enregistrer votre écran sous Windows** :hammer:
 

C’est un logiciel open source, présent Windows uniquement, qui permet d’enregistrer son écran pour en faire des GIFs animés, des MP4 ou simplement des captures écrans au format PNG ou JPEG.

Il y plusieurs options comme la possibilité de masquer le curseur de la souris lors des enregistrements, de choisir le taux de rafraichissement, de capter tout l’écran ou juste une partie, mais également de mettre un délai avant le lancement ou une durée sur l’enregistrement. Il est également possible d'annoter ou dessiner sur les captures écran assez simplement.


!!! note "Prise en main"
    - Lien vers l’outil sur le Windows Store : [https://apps.microsoft.com/detail/xp8cd3d3q50ms2?hl=en-US&gl=FR](https://apps.microsoft.com/detail/xp8cd3d3q50ms2?hl=en-US&gl=FR){:target="_blank" }
    - Lien vers un tutoriel :  A venir
