---
author: Christophe Bonvin
title:  🔨 Sauvegarde et synchronisation de fichiers, SyncBackFree
tags:
    - niveau intermédiaire 👌
    - version gratuite
    - inscription ✖️
---
:hammer: **SyncBackFree : Sauvegarde et synchronisation de fichiers** :hammer:
 

Vous avez créé un fichier sur votre PC personnel et l’avez mis sur votre clé USB pour l’utiliser en classe. Mais il y a une erreur et vous modifiez le fichier sur la clé. Mais le fichier qui est sur votre PC n’est pas modifié, vous avez deux versions différentes du même document. Il serait intéressant qu’en revenant chez vous, votre clé et votre PC se synchronisent pour ne garder que la version corrigée du fichier. Ceci est possible avec **SyncBackFree** (version gratuite). 
Vous pouvez soit créer des sauvegardes sur une clé ou un autre dossier (qui peut être un Drive) ou les synchroniser. Il est même possible de configurer le PC pour la synchronisation débute automatiquement quand on branche la clé USB.



!!! info "Remarque"
    Une version payante est disponible mais la version gratuite est largement suffisante.


!!! tnote "Prise en main"
    - Lien vers l’outil (version gratuite) : [https://www.2brightsparks.com/download-syncbackfree.html](https://www.2brightsparks.com/download-syncbackfree.html){:target="_blank" }
    - Lien vers un tutoriel :  [Tutoriel succint](https://ladigitale.dev/digiplay/#/v/6357b7879920d){:target="_blank" } -  [Tutoriel complet](https://sospc.name/syncback-free/){:target="_blank" }