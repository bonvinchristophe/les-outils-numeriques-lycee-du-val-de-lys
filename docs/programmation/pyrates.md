---
author: Christophe Bonvin
title:  🐍 Apprendre l'algorithmique et la programmation Python par le jeu, Py-Rates
tags:
    - exercice 📄
    - niveau débutant 👍
    - inscription ✖️
    
---
:snake: **Py-Rates : Apprendre l'algorithmique et la programmation Python par le jeu** :snake:
 
C’est un jeu développé dans le cadre d'une thèse en didactique de l'informatique. Il s'agit d'un site Internet accessible via un navigateur web et qui permet une première approche de la programmation en Python pour les élèves de seconde sous la forme d'un jeu de plateforme sur le thème des pirates. L'application est simple d'usage (page unique) et ne nécessite aucune inscription (conforme au RGPD).


!!! note "Prise en main"
    - Lien vers l’outil : [https://py-rates.fr/](https://py-rates.fr/){:target="_blank" }
    - Lien vers la documentation : [https://py-rates.fr/guide/FR/index.html](https://py-rates.fr/guide/FR/index.html){:target="_blank" }

!!! info "Remarque"
    Les solutions sont disponibles sur YouTube. Il faut bloquer l'accès au site pour éviter que les élèves ne les trouvent.
    

