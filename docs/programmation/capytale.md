---
author: Christophe Bonvin
title:  🐍 Travailler et évaluer la programmation, Capytale
tags:
    - exercice 📄
    - niveau intermédiaire 👌
    - ENT 🇫🇷
    
    
---
:snake: **Capytale : Travailler et évaluer la programmation** :snake:
 
**Capytale** est un outil développé par des enseignants de l’académie de Paris et d’Orléans (ce sont les mêmes collègues qui ont développé Basthon). Il est accessible pour les enseignants et les élèves dans l’ENT. 
Il permet de réaliser plusieurs types d’activité :

- un script Console en python
- un notebook en python
- créer des jeux en python
- du pixel art
- du bloc (comme Scratch)
- travailler le langage SQL et OCAMl (programme de NSI)
- travailler du HTML, CSS et JavaScript (programme de SNT)
- travailler sur des cartes électroniques (microbit, arduino, etc.)
- travailler avec des robots (Mbot, thymio, etc.)

Les enseignants peuvent créer des activités et les partager par code aux élèves. Les élèves rendent les copies numériques en fin de séance et l’enseignant récupère automatiquement les copies avec le nom des élèves.



!!! info "Remarque"
    L'accès à Capytale se fait via l'ENT. Il suffit de cliquer sur le connecteur. 
    Il existe dans Capytale, une bibliothèque d’activités créées par des collègues et peuvent servir de base.


!!! note "Prise en main"
    - Lien vers la documentation : [Utiliser Capytale](https://nuage03.apps.education.fr/index.php/s/j8E6mWTkZT2Lm2S){:target="_blank" }
