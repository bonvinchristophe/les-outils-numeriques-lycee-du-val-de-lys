---
author: Christophe Bonvin
title:  🐍 Travailler la programmation python en ligne, Basthon
tags:
    - exercice 📄
    - niveau débutant 👍
    - inscription ✖️
    
---
:snake: **Basthon : Travailler la programmation python en ligne** :snake:
 
Le site **Basthon.fr** permet de travailler le langage de programmation Python sans avoir besoin d’installer un interpréteur (ex : Edupython). Tout se passe en ligne sur le navigateur Web (Chrome ou Firefox conseillés). Il y a une grande quantité de modules déjà installés : numpy, matplotlib, maths, etc. On peut y joindre des documents pour y appliquer des opérations avec Python, on peut partager une fenêtre avec un code par lien (voir l’exemple ci-dessous). Il est possible de travailler directement depuis la console Python ou d’y faire des notebooks.


!!! note "Prise en main"
    - Lien vers l’outil : [https://basthon.fr/](https://basthon.fr/){:target="_blank" } - [Version Console](https://console.basthon.fr/){:target="_blank" } - [Version Notebook](https://notebook.basthon.fr/){:target="_blank" }
    - Lien vers le tutoriel : [Documentation](https://basthon.fr/doc.html){:target="_blank" }
    - Lien vers un [exemple](https://console.basthon.fr/?script=eJxLyy9SyFTIzFMoSsxLT1XQMDTQtOLlUgCCgqLMvBIFjUxNALfqCZ8){:target="_blank" }
