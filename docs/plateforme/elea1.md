---
author: Christophe Bonvin
title: 📓 Créer des parcours pédagogiques, Elea
tags:
    - niveau intermédiaire 👌
    - exercice 📄
    - inscription ✖️
    - ENT 🇫🇷
---

 :notebook:  **Elea : une plateforme d'apprentissage basée sur Moodle et comparable à Google Classroom** :notebook: 

La plateforme Moodle **Éléa** permet aux enseignants de créer des parcours pédagogiques scénarisés pour les élèves afin de mettre en œuvre certains principes de l’enseignement hybride comme la pédagogie inversée par exemple. Les élèves peuvent progresser à leur rythme grâce aux contenus pédagogiques proposés en ligne par leur enseignant.

Afin de favoriser l’engagement et la motivation des élèves pour réaliser le travail proposé en ligne, la plateforme Éléa offre la possibilité d’intégrer la gamification aux situations d’apprentissage, en reprenant des mécanismes empruntés aux jeux vidéos (carte de progression, défis, obtention de récompenses ou de badges, ...).

La mutualisation étant un concept essentiel en matière de pédagogie, des parcours-ressources validés par les corps d’inspection sont mis à disposition des enseignants ayant envie de découvrir les fonctionnalités de la plateforme. Ils peuvent les consulter dans la Éléathèque, se les approprier et les adapter en fonction de leurs besoins.

Pour accéder à la plateforme Éléa, les élèves se connectent à leur ENT. Une fois authentifiés, ils arrivent sur leur espace où sont listés leurs dossiers et les parcours auxquels ils sont inscrits.
Les enseignants disposent de droits supplémentaires. Ils peuvent créer leur premier parcours en utilisant les gabarits pré-construits. Ils ont également accès à la Éléathèque et aux tutoriels si besoin.
L’interface a été simplifiée et pensée pour faciliter l’utilisation de la plateforme. Un travail sur l’ergonomie a été réalisé afin de rendre la prise en main de l’outil la plus intuitive possible.

!!! tip "Prise en main"
    - Lien vers l’outil : Accéder à l'application **ELEA** depuis l'ENT
    - Lien vers le tutoriel : Une [page](https://ressources.dane.ac-versailles.fr/spip.php?page=tutoriel&id_ressource=126){:target="_blank" } avec différents tutoriels pour la prise en main et la conception des activités