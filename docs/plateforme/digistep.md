---
author: Christophe Bonvin
title: 📓 Créer des parcours pédagogiques, Digisteps
tags:
    - niveau avancé 💪
    - exercice 📄
    - inscription ✖️
    - la digitale 🌺
---

 :notebook:  **DigiSteps : Créer des parcours pédagogiques** :notebook: 

**Digisteps** a pour objectif de répondre à un besoin simple : offrir aux formateurs et aux enseignants qui n'ont pas accès à une plateforme de gestion des apprentissages (LMS) la possibilité de créer des parcours pédagogiques simples à partager avec les apprenants. L'outil permet ainsi d'agréger, au sein d'une même page, différents ingrédients pédagogiques :
    • des séances en présence ou à distance ;
    • des documents (fichiers et liens) ;
    • des exercices (fichiers et liens) ;
    • des activités (fichiers et liens, avec ou sans évaluation).
Les parcours générés avec Digisteps peuvent servir de fil conducteur lors d'une formation ou d'une séquence pédagogique. En activant l'option de dépôt pour les activités, les apprenants peuvent également transmettre leurs travaux, sans créer de compte. Les étapes sont visibles par défaut par tous les apprenants, mais il est possible de les masquer et de les rendre disponibles au fur et à mesure de l'avancée dans le parcours.
Un système de code d'accès permet également de masquer le contenu d'une étape, ce qui permet, par exemple, d'utiliser Digisteps pour des parcours ludiques, de type chasse au trésor, jeu de piste, jeu d'évasion, etc.

 
!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digisteps/](https://ladigitale.dev/digisteps/){:target="_blank" }
    - Lien vers le tutoriel : [Vidéo 1](https://ladigitale.dev/digiplay/#/v/6357dc9d4d2b7){:target="_blank" } - [Vidéo 2](https://ladigitale.dev/digiplay/#/v/6357dc50dbe65){:target="_blank" }