---
author: Christophe Bonvin
title:  😒 Faire des cartes (flash-cards) pour mémoriser, Digiflashcards
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---
 :unamused:  **DigiFlashcard : Faire des cartes pour mémoriser** :unamused: 

**Digiflashcards** est une application libre et gratuite de la plateforme **Ladigitale.dev.**

Cette application permet de créer facilement et gratuitement des paquets de cartes de "mémorisation active" sous forme de questions / réponses, éventuellement enrichies d’illustrations ("indice de récupération" pour la mémoire) et d’enregistrements audio courts. 

Un mode quiz automatique permet également à l’élève de se tester. 

DIGIFLASCARDS fonctionne très bien sur ordinateur, tablette et smartphone. Les paquets de cartes sont faciles à transmettre aux élèves par lien, QR Code ou lien d’intégration iframe. Un outil, pratique et respectueux des données des utilisateurs.

!!! danger "Accéder à ses flashcards"
    Pour accéder ultérieurement à vos flashcards, il faut mémoriser son adresse en l’ajoutant par exemple dans vos favoris. Le mot de passe créé au début de la création des flashcards sera nécessaire pour une modification ultérieure, pensez à le noter.

!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digiflashcards/](https://ladigitale.dev/digiflashcards/#/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiflashcards](https://ladigitale.dev/digiplay/#/v/6356974bd01c6){:target="_blank" }
    - Exemple de flashcards : [Exemple en SNT](https://ladigitale.dev/digiflashcards/#/f/630e2ae2c1154){:target="_blank" }
    
!!! info "Remarque"
    D'autres outils existent comme Anki, Quizlet, etc. Mais Digiflashcards est entièrement libre et gratuit sans aucune restriction. Un comparatif d'autres outils est disponible [ici](https://ciel.unige.ch/2016/10/choisir-le-bon-outil-pour-creer-ses-flashcards/){:target="_blank" }