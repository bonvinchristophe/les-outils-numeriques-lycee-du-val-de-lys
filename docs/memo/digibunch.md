---
author: Christophe Bonvin
title:  😒 Centraliser des liens (bouquet de liens), Digibunch  
tags:
    - mettre des ressources à disposition 🔗
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---
 :unamused:  **Digibunch : créer un bouquet de liens** :unamused: 

**Digibunch** est une application libre et gratuite de la plateforme Ladigitale.dev. 

Ce module, qui ne nécessite pas de création de compte, permet de créer des pages de liens que vous pouvez ensuite partager. A la création, on vous demande de choisir une question avec une réponse que vous êtes le seul à connaitre, ce qui vous permettra de reprendre la main sur votre DIGIBUNCH pour le modifier.

!!! danger "Accéder à ses bouquets de liens"
    Pour accéder ultérieurement à votre bouquet de lien, il faut mémoriser son adresse en l’ajoutant par exemple dans vos favoris. Le mot de passe créé au début de la création du bouquet de lien sera nécessaire pour une modification ultérieure, pensez à le noter.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digibunch/#/](https://ladigitale.dev/digibunch/#/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digibunch](https://ladigitale.dev/digiplay/#/v/6357d272d7167){:target="_blank" }
    - Un exemple d'utilisation en SNT [sur le thème 1 : le WEB](https://ladigitale.dev/digibunch/#/b/62bd622c73882){:target="_blank" }
       