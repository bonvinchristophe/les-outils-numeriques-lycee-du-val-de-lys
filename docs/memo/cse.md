---
author: Christophe Bonvin
title:  😒 Créer un moteur de recherche personnalisé, CSE  
tags:
    - mettre des ressources à disposition 🔗
    - niveau intermédiaire 👌
    - inscription ✖️
    
---
 :unamused:  **CSE : créer un moteur de recherche personnalisé** :unamused: 

**CSE** est une application libre et gratuite créée par Cedric Eyssette (enseignant de philosophie)
Cet outil vous permet de créer un moteur de recherche en limitant l'accès des élèves aux seuls sites que vous aurez autorisés. 
Le problème, quand on apprend aux élèves à travailler avec des moteurs de recherche, est l'exhaustivité des sources qui appparaissent, le tri peut être difficile, notamment pour de jeunes élèves. Avec cet outil, vous mettez quelques sites en référence et la recherche se fera à partir de ces sites. Il est plus facile de travailler avec les élèves sur le tri et la pertinence des informations.


!!! note "Prise en main"
    - Lien vers le tutoriel : [Utiliser CSE](https://cse.forge.apps.education.fr/){:target="_blank" }
    - Un exemple d'utilisation [en philosophie](https://cse.forge.apps.education.fr/#https://eyssette.forge.apps.education.fr/my-cse/intro-philo.md){:target="_blank" }
    - Un exemple d'utilisation en [SVT](https://cse.forge.apps.education.fr/#https://codimd.apps.education.fr/krUWrUw8QKyQGCz61rKL2A?both){:target="_blank" }

