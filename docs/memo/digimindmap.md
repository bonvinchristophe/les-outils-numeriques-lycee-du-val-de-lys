---
author: Christophe Bonvin
title:  😒 Créer une carte mentale, Digimindmap
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---
 :unamused:  **Digimindmap : Créer une carte mentale** :unamused: 

**Digimidmap** est une application libre et gratuite de la plateforme Ladigitale.dev (créée par Emmanuel Zimmert) qui permet de créer des cartes heuristiques ou cartes mentales.

Ses utilisations sont variées : la prise de note ; la préparation d'un exposé ; le remue-méninge ; l'aide au résumé ; la structuration d'un projet ; la révision à partir de mots clés ; l'organisation d'idées ; etc. Digimindmap propose de créer des cartes heuristiques simples, composées de texte. Les productions peuvent être enregistrées et partagées en ligne ou exportées en image (capture d'écran).

L'outil n'a pas été conçu pour réaliser des cartes complexes, mais plutôt pour créer simplement et rapidement un document dans le cadre d'une activité en présence ou à distance. Chaque item bénéficie d'un champ de notes, ce qui permet d'ajouter des informations complémentaires.

!!! danger "Accéder à ses bouquets de liens"
    Pour accéder ultérieurement à votre bouquet de lien, il faut mémoriser son adresse en l’ajoutant par exemple dans vos favoris. Le mot de passe créé au début de la création du bouquet de lien sera nécessaire pour une modification ultérieure, pensez à le noter.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digimindmap/#/](https://ladigitale.dev/digimindmap/#/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digimindmap](https://ladigitale.dev/digiplay/#/v/6356c5019a773){:target="_blank" }
    - D'autres logiciels et sites plus complets existent pour créer des cartes mentales comme FreeMind, X-Mind, Framindmap

!!! info "Remarque"
    Une application **carte mentale** existe dans l'ENT et permet de partager et de collaborer sur des cartes mentales
