---
author: Christophe Bonvin
title:  😒 Créer des pads (alternative gratuite à Padlet), Digipad
tags:
    - mettre des ressources à disposition 🔗
    - niveau débutant 👍
    - inscription ✔️
    - la digitale 🌺
---
 :unamused:  **Digipad : Créer des pads (alternative à Padlet)** :unamused: 

**Digipad** est une application libre et gratuite de la plateforme Ladigitale.dev. 

C’est est un mur collaboratif qui ressemble à Padlet mais qui est gratuit, libre et très complet : dépôts de fichiers (25 Mo par fichier, pas de limite en nombre de dépôts), mode écriture ou simple lecture, intégration de liens (sites, vidéos...), partage facile, pas de création de compte par les élèves, pas de restriction en nombre de pads créés... 

Contrairement à Padlet, il n'y a pas de limite de nombre de mur. Il est possible de collaborer avec des collègues.


!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.


!!! note "Prise en main"
    - Lien vers l’outil : [https://digipad.app/](https://digipad.app/){:target="_blank" }
    - Lien vers des tutoriels :
        - [Présentation (version courte)](https://ladigitale.dev/digiplay/#/v/635694740db45){:target="_blank" }
        - [Présentation (version complète)](https://ladigitale.dev/digiplay/#/v/6356949aef852){:target="_blank" }
        - [Nouvelles options](https://ladigitale.dev/digiplay/#/v/635694be010e9){:target="_blank" }
    - Lien vers des exemples de pad :
        - [Ressources pour réviser des chapitres en terminale spécialité](https://digipad.app/p/197583/13005cb88ebd8){:target="_blank" }
        - [Présentation des éprueves du bac](https://digipad.app/p/223967/56d05ca1f5d41){:target="_blank" }
        - [Plan de travail en SNT](https://digipad.app/p/469303/a241b1e8d9707){:target="_blank" }

