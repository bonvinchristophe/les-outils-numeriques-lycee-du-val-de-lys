---
author: Christophe Bonvin
title: 💻 Gestion de classe, Tableau en sciences
tags:
    - niveau débutant 👍
    - inscription ✖️
    - vpi 💡

---
 :computer: **Tableau en sciences : Gestion de classe**:computer:

**tableau.ensciences.fr** est un outil similaire à Digiscreen mais fournit un peu moins de fonctionnalités que Digiscreen. Il possède les fonctionnales de base avec quelques autres plus poussées pour les sciences : Editeur d'équations, Editeur de molécules, GeoGebra, Console Python et Calculatrice (Ti83 et Numworks)


!!! info "Remarque"
    Pour l'instant il ne semble pas possible d'exporter et d'enregistrer ses "screens". L'auteur est en train d'y travailler.


!!! note "Prise en main"
    - Lien vers l’outil : [https://tableau.ensciences.fr/](https://tableau.ensciences.fr/){:target="_blank" }
    

