---
author: Christophe Bonvin
title:  💻 Tableau blanc intéractif, OpenBoard (VPI)
tags:
    - niveau intermédiaire 👌
    - inscription ✖️
    - vpi 💡
---
 :computer: **OpenBoard : Tableau blanc interactif** :computer:

**OpenBoard** est un logiciel installé sur tous les PC profs du lycée. Il permet de créer des diapositives avec des documents numériques (vidéos, images, textes, etc.) et de les annoter avec les stylets du VPI. Il propose des outils d’annotation, de dessin, de géométrie pour réaliser vos figures et votre trace écrite. Les diapositives peuvent être préparées à l’avance (en amont de la séance). Une fois le cours réalisé, le tout peut être exporté au format PDF pour être joint au cahier de texte par exemple.

!!! note "Prise en main"
    - Lien vers l’outil : [https://openboard.ch/download.html](https://openboard.ch/download.html){:target="_blank" }
    - Lien vers la documentation : [toutes les fonctionnalités d’OpenBoard](https://openboard.ch/download/Tutoriel_OpenBoard_1.6.pdf){:target="_blank" }
    - Lien vers la documentation simplifiée : [Partie 1](https://openboard.ch/download/OpenBoard1.6_interface.pdf){:target="_blank" } – [Partie 2](https://openboard.ch/download/OpenBoard1.6_Mode_Doc_en_1_page.pdf){:target="_blank" }
    - Lien vers un tutoriel vidéo : [Prise en main](https://ladigitale.dev/digiplay/#/v/63568906a8b20){:target="_blank" } – [Les outils en géométrie](https://ladigitale.dev/digiplay/#/v/63568b1938a27){:target="_blank" } – [Les applications dans OpenBoard](https://ladigitale.dev/digiplay/#/v/63568b65100fb){:target="_blank" } – [Annoter une document PDF](https://ladigitale.dev/digiplay/#/v/63568bd397b03){:target="_blank" } – [Exporter sa présentation en pdf](https://ladigitale.dev/digiplay/#/v/63568c0f58bb3){:target="_blank" } – [Bien utiliser OpenBoard](https://ladigitale.dev/digiplay/#/v/63568e1c83ce5){:target="_blank" } – [Gérer ses classes et ses documents](https://ladigitale.dev/digiplay/#/v/63568e7665c73){:target="_blank" } – [Annoter un tableau](https://ladigitale.dev/digiplay/#/v/63568ea3f24a6){:target="_blank" }
