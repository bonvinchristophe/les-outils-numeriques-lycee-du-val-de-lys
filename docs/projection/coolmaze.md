---
author: Christophe Bonvin
title:  💻 Transférer un média (photo ou vidéo) directement de votre smartphone vers l’écran, Cool Maze
tags:
    - niveau débutant 👍
    - inscription ✖️
    - smartphone 📱
    - vpi 💡
   
---
 :computer: **Cool Maze : Transférer un média (photo ou vidéo) directement de votre smartphone vers l’écran** :computer:

**Cool Maze** est une application pour smartphone qui vous permet en moins de 10s d’afficher une photo (ou une vidéo) de votre téléphone sur votre ordinateur (et donc sur le vidéoprojecteur qui lui est relié). 

_Possibilités_ : 
- afficher un travail d’élève au tableau
- afficher une page de livre ou d’un document au tableau
- demander aux élèves de prendre une photo d’une expérience, ils peuvent ensuite la télécharger sur leur PC et la travailler

!!! note "Prise en main"
    - Lien vers l’outil : [https://coolmaze.io/](https://coolmaze.io/){:target="_blank" }
    - Lien vers l’appli (Android) : [https://play.google.com/store/apps/details?id=com.bartalog.coolmaze](https://play.google.com/store/apps/details?id=com.bartalog.coolmaze){:target="_blank" }
    - Lien vers l’appli (iOS) : [https://apps.apple.com/us/app/cool-maze/id1284597516](https://apps.apple.com/us/app/cool-maze/id1284597516){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Cool Maze](http://lnb.svt.free.fr/nouveau lycee/-document/FM/FT L27 Tuto-Cool-Maze.pdf){:target="_blank" }

