---
author: Christophe Bonvin
title: 💻 Gestion de classe, Digicreen
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
    - vpi 💡
     
---
 :computer: **Digiscreen : Gestion de classe** :computer:

**Digiscreen** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Il permet de créer un tableau pour la gestion de classe avec différents outils : chronomètre, minuteur, documents à projeter, tirage au sort, générateur de groupe, générateur de QR Code, etc.

Il est possible de faire ses "screens" à l'avance, de les enregistrer sur un support (clé USB, espace de travail) et de les ouvrir par la suite.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digiscreen/](https://ladigitale.dev/digiscreen/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiscreen](https://ladigitale.dev/digiplay/#/v/634d72dc14c81){:target="_blank" }
    - Lien vers un [fichier test à télécharger](https://nuage03.apps.education.fr/index.php/s/6QGMjgmqJiLtyfd){:target="_blank" } et à importer dans Digiscreen
