---
author: Christophe Bonvin
title:  👬 Stocker et partager des documents ,Nuage
tags:
    - niveau débutant 👍
    - Apps Education 🇫🇷
    - exercice 📄
---
 :two_men_holding_hands: **Nuage d’Apps Education : Stocker et partager des documents** :two_men_holding_hands:
 

**Nuage** est une application comme Google Drive ou Dropbox, mais ici l’outil est hébergé par l’Education Nationale et vous propose un espace de stockage jusque 100Go. Pour y accéder il faut un compte sur Apps Education. 

Il permet de créer des documents en ligne (traitement de texte, tableur, diaporama) et de les partager avec des collègues ou des élèves. Il y a des règles qui permettent de donner le droit en lecture, et/ou en écriture (modification) sur les fichiers, de mettre une date d'expiration, un mot de passe, etc.

Ce service se revèle très pratique pour créer un espace de dépôt de travaux pour les élèves sans que ces derniers ne peuvent consulter ceux des camarades. (voir l'exemple ci-dessous)

!!! note "Prise en main"
    - Lien vers l’outil : [https://portail.apps.education.fr/](https://portail.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Nuage](https://nuage03.apps.education.fr/index.php/s/LfwR6rkTAQ94m95){:target="_blank" }

??? info "Remarque - Inscription Apps Education"
    Une inscription est nécessaire pour l'enseignant. Voir ce [tutoriel](https://nuage03.apps.education.fr/index.php/s/DdPsmgzxtnABa3A){:target="_blank" } pour créer son compte Apps Education


!!! info "Exemple : Espace de dépôt"
    - [Lien à communiquer](https://nuage03.apps.education.fr/index.php/s/xXHpNrQ7BCRNiM7){:target="_blank" } aux élèves pour déposer le travail
    - Pour ce même dossier on peut générer un [lien différent à communiquer](https://nuage03.apps.education.fr/index.php/s/eTHmJeHLaNJyR9S){:target="_blank" } à un collègue pour avoir accès aux travaux.
    - Essayer de téléverser un fichier comme un élève, et cliquer sur le lien "vision prof" pour vérifier qu'il a bien été ajouté. Vous pouvez le modifier et le supprimer.