---
author: Christophe Bonvin
title:  👬 Stocker et partager des documents, Espace documentaire
tags:
    - niveau débutant 👍
    - ENT 🇫🇷
---
 :two_men_holding_hands: **Espace documentaire : Stocker et partager des documents** :two_men_holding_hands:
 

L’application **espace documentaire** est une application accessible sur l’ENT pour les élèves comme les enseignants. Elle permet comme un « cloud » de stocker des fichiers personnels dans des dossiers (capacité de stockage de 4Go). Il est possible de créer un dossier et de le partager avec des collègues et/ou des élèves pour partager des documents


!!! note "Prise en main"
    - Lien vers le tutoriel vidéo : [Présentation de l'espace documentaire](https://drive.google.com/file/d/1o8vNX3s4zcPn7XGfXbdHpQaKkbFb8BqG/view?usp=sharing){:target="_blank" }