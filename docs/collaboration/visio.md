---
author: Christophe Bonvin
title:  👬 Faire des visio-conférence, Visio-Agents
tags:
    - niveau débutant 👍
    - Apps Education 🇫🇷
---
 :two_men_holding_hands: **Visio-Agents : Faire des visio-conférence** :two_men_holding_hands:
 

**Visio Agent** est une solution de visioconférence basée sur BBB (BigBlueButton) et hébergée par l’Etat (conforme au RGPD). Elle permet de créer ou non une salle d’attente, de valider ou non certains participants sans que ces derniers n’aient besoin de créer un compte.

!!! note "Prise en main"
    - Lien vers l’outil : [https://portail.apps.education.fr/](https://portail.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Démarrer avec Visio Agents](https://ww2.ac-poitiers.fr/dane/spip.php?article947){:target="_blank" }

??? info "Remarque - Inscription Apps Education"
    Une inscription est nécessaire pour l'enseignant. Voir ce [tutoriel](https://nuage03.apps.education.fr/index.php/s/DdPsmgzxtnABa3A){:target="_blank" } pour créer son compte Apps Education