---
author: Christophe Bonvin
title:  👬 Transférer des fichiers volumineux, France Transfert
tags: 
    - niveau débutant 👍
    - Apps Education 🇫🇷
---
 :two_men_holding_hands: **France Transfert : Transférer des fichiers volumineux** :two_men_holding_hands:
 
**France transfert** est un nouveau service créé par l’État qui permet d'envoyer des fichiers volumineux non sensibles de manière sécurisée à un agent de l'État ou entre agents. Cette nouvelle application vient en complément de FileSender (RENATER) qui permet de partager des fichiers volumineux de plusieurs giga-octets de façon sécurisée.

France transfert permet d’envoyer jusqu’à 20 Go de fichiers et dossiers par transfert avec une limite de 2 Go maximum par fichier. 

    
!!! note "Prise en main"
    - Lien vers l’outil : [https://portail.apps.education.fr/](https://portail.apps.education.fr/){:target="_blank" }
    
??? info "Remarque - Inscription Apps Education"
    Une inscription est nécessaire pour l'enseignant. Voir ce [tutoriel](https://nuage03.apps.education.fr/index.php/s/DdPsmgzxtnABa3A){:target="_blank" } pour créer son compte Apps Education
