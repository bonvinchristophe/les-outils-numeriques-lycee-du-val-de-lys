# Des outils pour l'audio :sound: 

### :one: Digirecord : Enregistrer et partager des vidéos
**Digirecord** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
C’est un outil qui permet de convertir des fichiers audio et des vidéos directement depuis votre navigateur web sans aucun logiciel à installer. Ceci se révèle particulièrement intéressant dans le cadre de travaux rendus par les élèves sous forme de vidéo ou d'audio dans des formats peu connus.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digirecord/](https://ladigitale.dev/digirecord/#/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digirecord](https://ladigitale.dev/digiplay/#/v/634d6c1311481){:target="_blank" }

### :two: MonOral.net : Travailler et évaluer l’oral à la maison

**Mon oral.net** est un outil qui permet pour l’enseignant de créer des activités et des entrainements pour une ou des classes. L’enseignant reçoit tous les fichiers dans un même dossier.
Il permet de faire travailler les élèves au Grand Oral, ou de faire des corrections audio qui sont ensuite exportées sous forme de **QR-Code**.


!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://www.mon-oral.net/](https://www.mon-oral.net/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digirecord](https://ladigitale.dev/digiplay/#/v/634d6c1311481){:target="_blank" }


### :three: Digicut et Digitranscode : Couper et convertir des fichiers audio
Ces outils sont présentés dans le chapitre "Des outils pour la vidéo".  Vous pouvez y accéder avec ces liens :  

- pour accéder à [Digicut](https://bonvinchristophe.forge.apps.education.fr/outils-num-vdl/video/#digicut-couper-une-video-et-aussi-un-audio)  pour couper un fichier audio

- pour accéder à [Digitranscode](https://bonvinchristophe.forge.apps.education.fr/outils-num-vdl/video/#digitranscode-convertir-des-fichiers-video-en-ligne) pour convertir un fichier audio



