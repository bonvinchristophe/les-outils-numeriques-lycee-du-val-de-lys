---
author: Christophe Bonvin
title:  🎥 Plateforme de dépôt de vidéos, Podeduc et Portail Tubes 
tags:
    - niveau intermédiaire 👌
    - inscription ✔️
    - Apps Education 🇫🇷
---
 :movie_camera: **Publier une vidéo sur une plafeforme (streaming vidéo)** :movie_camera: 

Deux alternatives à YouTube de Google existent. Elles présentent l'avantage d'héberger les vidéos en France sur des serveurs du ministère de l'éductaion nationnale
!!! danger "Droit à l'image"
    - Même si ces plateformes sont hébergées en France et sont institutionnelles, elles ne dispensent de faire une demande d'autorisation de prise de vue et de publication (droit à l'image) auprès de l'élève et de ses parents s'il est mineur.
    - Il convient de bien choisir les paramètres de diffusion et de supprimer les contenus dans lesquels les élèves apparaissent au bout d'un an (droit à l'oubli)
Voir le [tutoriel](https://nuage03.apps.education.fr/index.php/s/LREbY3BcFt4QLej){:target="_blank" } pour accéder à Apps – Education

#### :arrow_forward: Portails Tubes
Ce service est destiné à héberger des vidéos que vous avez créées (capsules, production élèves, etc.) sans les héberger sur une plateforme tierce.
Une fois la vidéo en ligne, on peut rajouter des sous-titres, générer un QR-code, créer un lien de partage (avec des droits d’accès), etc.

!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.

!!! note "Prise en main"
    - Fonctionnement du portail Tubes : [voir la vidéo](https://tube-numerique-educatif.apps.education.fr/w/koKWnHzNAqntRxG3wdvLyV){:target="_blank" }
    - Exemple de vidéo hébergée sur Tubes :  [présentation de apps education](https://tube-numerique-educatif.apps.education.fr/w/ekMHNQk3t5ShDheocbwxsV){:target="_blank" }
  
#### :arrow_forward: Pod Educ
Ce service est plutôt destiné à héberger des vidéos entre collègues et institution (magistère, Visio agents, etc.)
!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.


!!! note "Prise en main"
    - Lien vers l’outil : [https://podeduc.apps.education.fr/](https://podeduc.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Publier une vidéo](https://podeduc.apps.education.fr/video/0052-ajouter-une-video/){:target="_blank" }
    - Une page avec [différents tutoriels](https://podeduc.apps.education.fr/tutoriels-pod-educ/){:target="_blank" } pour présenter les différentes fonctionnalités de Pod Educ