---
author: Christophe Bonvin
title:  🎥 Enregistrer son écran, Podeduc 
tags:
    - niveau intermédiaire 👌
    - inscription ✔️
    - Apps Education 🇫🇷
---
 :movie_camera: **Pod Educ : Enregistrer son écran** :movie_camera: 

Pour utiliser **Pod Educ**, il faut activer son compte _Apps-Education_ (service d’applications gratuit et hébergé par le ministère de l’Education nationale). 
!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.
Voir le [tutoriel](https://nuage03.apps.education.fr/index.php/s/LREbY3BcFt4QLej){:target="_blank" } pour accéder à Apps – Education

!!! note "Prise en main"
    - Lien vers l’outil : [https://podeduc.apps.education.fr/](https://podeduc.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Enregistrer son écran](https://podeduc.apps.education.fr/video/0128-le-nouvel-enregistreur/){:target="_blank" }
    - Une page avec [différents tutoriels](https://podeduc.apps.education.fr/tutoriels-pod-educ/){:target="_blank" } pour présenter les différentes fonctionnalités de Pod Educ

