---
author: Christophe Bonvin
title:  🎥 Récupérer une vidéo sur une plateforme (ex :Youtube)
tags:
    - niveau débutant 👍
    - inscription ✖️
    
---
 :movie_camera: **ytDownloader : Récupérer une vidéo sur une plateforme** :movie_camera: 

**ytDownloader** un outil puissant et polyvalent qui vous facilitera la vie en vous permettant de télécharger des vidéos et d’extraire des pistes audios de différents formats à partir de centaines de sites, dont, YouTube, Facebook, Tiktok, Twitch, Twitter, Instagram et bien d’autres…

L'interface est minimaliste et facile à prendre en main et l'outil est gratuit et Open Source disponible pour Windows, Mac et Linux

Cet outil dispose de nombreuses fonctionnalités comme un mode clair/sombre, la possibilité de choisir un thème pour le confort visuel, ainsi que la possibilité de télécharger une plage spécifique d’une vidéo si vous le souhaitez, ce qui est pratique pour extraire un morceau précis. Vous pouvez évidemment, après avoir entré l’URL de votre vidéo, choisir le format et la qualité de la vidéo et de l’audio que vous voulez récupérer.

Il prend également en charge le téléchargement des sous-titres et est disponible en plusieurs langues. Vous pouvez bien sûr configurer l’emplacement où enregistrer vos fichiers récupérés et il n’y a ni pubs ni traqueur dans l’application.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ytdn.netlify.app/](https://ytdn.netlify.app/){:target="_blank" }
    - Lien vers le tutoriel vidéo : A venir prochainement
