---
author: Christophe Bonvin
title:  🎥 Couper une vidéo (et aussi un audio), Digicut 
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---
 :movie_camera: **Digicut : Couper une vidéo (et aussi un audio)** :movie_camera: 

**Digicut** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
C’est un outil qui permet de d’extraire des morceaux d’une vidéo ou d’un audio directement depuis votre navigateur web sans aucun logiciel à installer. Simple et efficace !!

!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicut/](https://ladigitale.dev/digicut/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digicut](https://ladigitale.dev/digiplay/#/v/634d697c33c9d){:target="_blank" }
   
