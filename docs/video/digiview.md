---
author: Christophe Bonvin
title:  🎥 Partager et visionner une vidéo YouTube sans les publicités, Digiview 
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---
 :movie_camera: **Digiview : Partager et visionner une vidéo YouTube sans les publicités** :movie_camera: 

**Digiview** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
Vous avez trouvé une vidéo sur YouTube mais lors de la diffusion, il y a des publicités qui apparaissent au début voire au milieu de la lecture. Il est possible de les supprimer via l’outil Digiview (appartenant au site La [Digitale](https://ladigitale.dev/){:target="_blank" }). Aucune inscription, ni installation ne sont requises, tout se fait en ligne. Il y a possibilité de générer un **QR-code** et un **lien de partage**, de démarrer ou arrêter la vidéo à un moment précis


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digiview/#/](https://ladigitale.dev/digiview/#/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiview](https://ladigitale.dev/digiplay/#/v/634cff9e1cc02){:target="_blank" }
   
