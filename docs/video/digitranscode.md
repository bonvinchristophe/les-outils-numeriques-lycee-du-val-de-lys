---
author: Christophe Bonvin
title:  🎥 Convertir des fichiers vidéo en ligne, Digitranscode 
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---
 :movie_camera: **Digitranscode : Convertir des fichiers vidéo en lign** :movie_camera: 

**Digitranscode** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
C’est un outil qui permet de convertir des fichiers audio et des vidéos directement depuis votre navigateur web sans aucun logiciel à installer. Ceci se révèle particulièrement intéressant dans le cadre de travaux rendus par les élèves sous forme de vidéo ou d'audio dans des formats peu connus.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digitranscode/](https://ladigitale.dev/digitranscode/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digitranscode](https://ladigitale.dev/digiplay/#/v/634d6a15adfba){:target="_blank" }