---
author: Christophe Bonvin
title:  👷  Centralisation des ressources sur le site du lycée
tags:
    - niveau débutant 👍
    - utilisation facile 1️⃣
    - inscription ✖️
    
---
 :construction_worker: **Site du lycée : Centraliser les informations** :construction_worker:
 

Le site du lycée possède une rubrique **« Orientation »** en page d’accueil où sont regroupées différentes informations actualisées sur l’orientation : portes ouvertes, salons, parcours sup, immersion etc

!!! note "Prise en main"
    - Lien vers la ressource : [https://lpo-du-val-de-lys-estaires.59.ac-lille.fr/lorientation/](https://lpo-du-val-de-lys-estaires.59.ac-lille.fr/lorientation/){:target="_blank" }