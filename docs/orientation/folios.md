---
author: Christophe Bonvin
title:  👷 Porte document numérique, Folios
tags:
    - ENT 🇫🇷
    - niveau débutant 👍
---
 :construction_worker: **Folios : Porte document numérique** :construction_worker:
 

**Folios** est une application accessible par les enseignants et les élèves depuis l’ENT. Il permet aux élèves de stocker, classer et conserver des documents, des formulaires en lien avec l’orientation de la 6ème à la Terminale

!!! info "Remarque"
    L'accès à folios se fait via l'ENT. Il suffit de cliquer sur le connecteur. 
   

!!! note "Prise en main"
    - Lien vers le tutoriel : [Documentation](https://view.genial.ly/63334e50c7b5620018b7e641){:target="_blank" }
    
!!! danger "**Fermeture de FOLIOS**"
    - Folios n'est plus mis à jour et sera remplacé à partir de Décembre 2024 par un autre outil **Avenir(s)**
    - Il est possible de récupérer les informations sur Folios jusqu'au 31/12/2024
    - Plus d'informations dans le [document joint](https://folios.onisep.fr/servlet/com.univ.collaboratif.utils.LectureFichiergw?ID_FICHIER=8607685){:target="_blank" } et sur [https://avenirs.onisep.fr/](https://avenirs.onisep.fr/){:target="_blank" }
