---
author: Christophe Bonvin
title:  👷 Centraliser et catégoriser les informations, Digipad
tags:
    - niveau débutant 👍
    - inscription ✔️
    - la digitale 🌺
---
 :construction_worker: **Digipad : Centraliser et catégoriser les informations** :construction_worker:
 

**Digipad** permet de créer des murs avec différentes informations. Les tutoriels pour la prise en main de l'outil sont disponibles  [à cette adresse](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/memo/digipad/){:target="_blank" }


!!! note "Prise en main"
    - Exemple de DigiPad créé pour [l'AP en terminale générale  :](https://digipad.app/p/849023/ec5e7693cd159){:target="_blank" }


