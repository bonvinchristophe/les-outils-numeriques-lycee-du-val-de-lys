---
author: Christophe Bonvin
title:  🔉 Récupérer la transcription audio d'une vidéo
tags:
    - niveau intermédiaire 👌
    - inscription ✖️


---

 :sound: **Vibe : Récupérer automatiquement les transcriptions audio des vidéos**:sound: 

**Vibe** est un nouvel outil open source de transcription audio multilingue. Il s'installe aussi bien sur Windows, MacOs ou Linux.

Pour réaliser ses tâches, il utilise l’IA Whisper, développé par OpenAI (entreprise qui développe ChatGPT). Ce modèle de reconnaissance vocale est capable de transcrire un nombre important de langues avec une grande précision, ce qui permet de faire de Vibe une véritable solution audio polyvalente avec de nombreuses fonctionnalités.

Vous pouvez par exemple transcrire des fichiers audio et vidéo par lots, prévisualiser le résultat en temps réel, exporter dans différents formats (SRT, VTT, TXT…), et même personnaliser les modèles selon vos besoins. Il fonctionne entièrement hors ligne, donc pas de risque que vos données sensibles se retrouvent dans les griffes des GAFAM. 


!!! info "Remarque"
    Ce logiciel s'installe sur PC et nécessite, lors de la première connexion, un accès à internet pour télécharger le modèle IA. Par la suite, il n'y a plus besoin d'utiliser Internet.


!!! note "Prise en main"
    - Lien vers l’outil : [https://thewh1teagle.github.io/vibe/](https://thewh1teagle.github.io/vibe/){:target="_blank" }
    - Lien vers la page GitHub du projet : [https://github.com/thewh1teagle/vibe?tab=readme-ov-file](https://github.com/thewh1teagle/vibe?tab=readme-ov-file){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digirecord](https://ladigitale.dev/digiplay/#/v/634d6c1311481){:target="_blank" }