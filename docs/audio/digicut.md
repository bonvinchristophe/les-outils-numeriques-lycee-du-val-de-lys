---
author: Christophe Bonvin
title: 🔉 Couper et convertir des fichiers audio, Digicut et Digitranscode
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---

 :sound: **Digicut et Digitranscode : Couper et convertir des fichiers audio**

Ces outils sont présentés dans le chapitre "Des outils pour la vidéo".  Vous pouvez y accéder avec ces liens :  

- pour accéder à [Digicut](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/video/digicut/)  pour couper un fichier audio

- pour accéder à [Digitranscode](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/video/digitranscode/) pour convertir un fichier audio


