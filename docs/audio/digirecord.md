---
author: Christophe Bonvin
title:  🔉 Enregistrer et partager des vidéos, Digirecord
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺
---

 :sound: **Digirecord : Enregistrer et partager des vidéos**:sound: 

**Digirecord** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
C’est un outil qui permet de convertir des fichiers audio et des vidéos directement depuis votre navigateur web sans aucun logiciel à installer. Ceci se révèle particulièrement intéressant dans le cadre de travaux rendus par les élèves sous forme de vidéo ou d'audio dans des formats peu connus.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digirecord/](https://ladigitale.dev/digirecord/#/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digirecord](https://ladigitale.dev/digiplay/#/v/634d6c1311481){:target="_blank" }