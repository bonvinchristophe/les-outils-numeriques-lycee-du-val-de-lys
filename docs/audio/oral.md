---
author: Christophe Bonvin
title:  🔉 Travailler et évaluer l’oral à la maison, Mon-oral.net
tags:
    - niveau débutant 👍
    - inscription ✔️
    - oral 👥

---

 :sound: **MonOral.net : Travailler et évaluer l’oral à la maison**:sound: 

**Mon oral.net** est un outil qui permet pour l’enseignant de créer des activités et des entrainements pour une ou des classes. L’enseignant reçoit tous les fichiers dans un même dossier.
Il permet de faire travailler les élèves au Grand Oral, ou de faire des corrections audio qui sont ensuite exportées sous forme de **QR-Code**.


!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.


!!! note "Prise en main"
    - Lien vers l’outil : [https://www.mon-oral.net/](https://www.mon-oral.net/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digirecord](https://ladigitale.dev/digiplay/#/v/634d6c1311481){:target="_blank" }