---
author: Christophe Bonvin
title:  📄 Tester les élèves et les évaluer en même temps, QCM
tags:
    - exercice 📄
    - niveau intermédiaire 👌
    - ENT 🇫🇷
---
 :page_facing_up: **QCM Pronote : Tester les élèves et les évaluer en même temps** :page_facing_up:
 

**Pronote** permet de créer des QCM à réaliser aux élèves. Il peut calculer automatiquement une note qui peut, ou non, compter dans la moyenne de l’élève. 

Différentes options sont disponibles : mettre un temps pour le QCM ou une question, mélanger les questions et les réponses, revenir en arrière ou pas, mettre des images, une vidéo ou un son dans la question.

Les QCM élaborés peuvent être partagés avec les collègues de l’établissement



!!! note "Prise en main"
    - Lien vers le site Index éducation. Sur cette page se trouvent de multiples tutoriels vidéo : [https://www.index-education.com/fr/tutoriels-video-pronote-686-29-utiliser-des-qcm-pour-evaluer-des-competences-espace-professeurs.php](https://www.index-education.com/fr/tutoriels-video-pronote-686-29-utiliser-des-qcm-pour-evaluer-des-competences-espace-professeurs.php){:target="_blank" }