---
author: Christophe Bonvin
title:  📄 Créer des QCM individualisés avec correction automatisée
tags:
    - exercice 📄
    - niveau avancé 💪
    - inscription ✖️
    - linux 🐧
    - LaTeX 📐

---

 :page_facing_up: **AMC : AutoMultipleChoice : Créer des QCM individualisés** :page_facing_up:

Ce logiciel est réservé aux utilisateurs.rices avancés.ées car il nécessite de maîtriser le langage Latex (ou de savoir modifier un fichier Latex à partir d'un modèle) et d’avoir un PC sous Linux ou MacOS. C’est un logiciel de création de devoir qui permet surtout de réaliser des QCM (avec 1 ou plusieurs réponses), mais il est également possible de créer des questions ouvertes.

Ses fonctionnalités permettent, à partir d’une banque de questions que vous avez constituée, de générer un devoir différent pour chaque élève.

Il est possible de paramétrer le logiciel pour corriger automatiquement les questions (cas du QCM) et de calculer automatiquement la note (dans le cas du QCM et de questions ouvertes).
 
!!! note "Prise en main"
    - Lien vers le site : [https://www.auto-multiple-choice.net/index.fr](https://www.auto-multiple-choice.net/index.fr){:target="_blank" }
    - Présentation succincte de la création d’un QCM (15min) : [https://ladigitale.dev/digiplay/#/v/635269f66810e](https://ladigitale.dev/digiplay/#/v/635269f66810e){:target="_blank" }
    - Lien tutoriel : [Partie 1](https://ladigitale.dev/digiplay/#/v/63526ac50346a){:target="_blank" } – [Partie 2](https://www.youtube.com/watch?v=gSVLQg-ZAQA){:target="_blank" } – [Partie 3](https://ladigitale.dev/digiplay/#/v/63526af918919){:target="_blank" }


??? warning "Installation sous Windows"
    Il n'existe pas de version Windows de ce logiciel mais un collègue a trouvé une parade. Tout est expliqué [ici](https://project.auto-multiple-choice.net/projects/auto-multiple-choice/wiki/Comment_installer_Ubuntu_et_le_logiciel_AMC_sous_Windows_avec_Oracle_VirtualBox){:target="_blank" }
