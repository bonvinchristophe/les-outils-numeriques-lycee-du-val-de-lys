---
author: Christophe Bonvin
title:  📄 Créer des exercices à rendre (QCM, texte à trous, etc.)
tags:
    - exercice 📄
    - niveau intermédiaire 👌
    - ENT 🇫🇷
---
 :page_facing_up: **Application “Exercice” de l’ENT : Créer des exercices à rendre** :page_facing_up:
 

L’application **Exercice** permet de réaliser des questionnaires à choix multiples comme Pronote. Un barème peut être donné et une note est alors attribuée.

Il est également possible de créer un exercice à rendre. L’outil permet de créer une consigne, et l’élève peut la consulter dans son application Exercice. Il y a un bouton sur sa page pour joindre son travail.

Pour les enseignants, il y a une interface qui fait le bilan des élèves qui ont rendu ou non le travail.


!!! note "Prise en main"
    - Vidéos tutoriel : 
        - [Partie 1](https://drive.google.com/file/d/19OOaenSZlaVXSopeJdKZC74jdNCemH9v/view?usp=sharing){:target="_blank" } 
        – [Partie 2](https://drive.google.com/file/d/19OOaenSZlaVXSopeJdKZC74jdNCemH9v/view?usp=sharing){:target="_blank" }