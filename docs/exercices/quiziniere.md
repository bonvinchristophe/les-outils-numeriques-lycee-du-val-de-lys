---
author: Christophe Bonvin
title:  📄 Créer des questionnaires en ligne, Quizinière
tags:
    - exercice 📄
    - niveau débutant 👍
    - inscription ✔️
    
    
---

 :page_facing_up: **Quizinière : Créer des questionnaires en ligne** :page_facing_up:

**Quizinière** permet aux enseignants de créer, de diffuser à leurs élèves et de corriger des exercices en ligne en toute simplicité. Cet outil propose de nombreuses activités : textes à trous, QCM, dessins, enregistrements audios, vidéos, formules mathématiques, etc.

Cet outil est hébergé et développé par CANOPE et est conforme au RGPD.

C'est un outil facile à prendre en main et qui dispose d'un catalogue de questionnaires réalisés par des collègues, que l'on peut réutiliser dans l'état ou adapter.
 
!!! note "Prise en main"
    - Lien vers l’outil : [https://www.quiziniere.com](https://www.quiziniere.com){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Quizinière](https://ladigitale.dev/digiview/#/v/67166d653eb45){:target="_blank" }

