---
author: Christophe Bonvin
title:  📄 Créer des exercices interactifs, Digiquiz et Logiquiz
tags:
    - exercice 📄
    - niveau avancé 💪
    - inscription ✖️
    - la digitale 🌺


    
---

 :page_facing_up: **Digiquiz et Logiquiz : Créer des exercices interactifs** :page_facing_up:

**H5P** (pour HTML5 Package) est une solution simple pour créer et utiliser des outils interactifs de type exerciseur en ligne. La plateforme la digitale vous propose deux outils complémentaires pour créer vos contenus.
 
!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/](https://ladigitale.dev/){:target="_blank" }
    - Lien vers le tutoriel : [Installer et utiliser Logiquiz](https://ladigitale.dev/digiplay/#/v/6357daa1636fb){:target="_blank" }

