---
author: Christophe Bonvin
title:  💾 Partager un lien avec un QR Code, Digicode
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺

---

 :arrow_forward::floppy_disk: **Digicode : Partager un lien avec un QR Code** :floppy_disk:

Le site la digitale regroupe un ensemble d’outils dont deux qui permettent de communiquer plus simplement des liens en créant un QR code à partir d'une adresse internet (url). La couleur peut être paramétrée. On peut ajouter une image ou un logo pour personnaliser le QR code. L’outil se nomme **Digicode**

!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicode/ ](https://ladigitale.dev/digicode/ ){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digicode](https://ladigitale.dev/digiview/#/v/63569ee06697f){:target="_blank" }
    
    
!!! info "Remarque"
    - L'outil digilink qui permet de raccourcir les liens n'est plus disponible sur la digitale (sauf en souscrivant à Digidrive pour 25€/an)



:arrow_forward::floppy_disk: **qrprint: Personnaliser et imprimer une planche de QrCodes** :floppy_disk:

Cette application permet de générer des QR Codes avec un titre personnalisé et offre la possibilité de générer une grille imprimable contenant plusieurs QR Codes à découper. Le style des QR Codes est entièrement personnalisable, y compris les couleurs, la forme des points, et les coins du code. L'application permet d'exporter la grille en PDF.

Il est possible de mettre différents QR codes sur la même planche (pour différencier des Qrcodes à différents élèves ou lors d'un travail en groupe
)
!!! note "Prise en main"
    - Lien vers l’outil : [https://qrprint.forge.apps.education.fr/app/ ](https://qrprint.forge.apps.education.fr/app/ ){:target="_blank" }
    