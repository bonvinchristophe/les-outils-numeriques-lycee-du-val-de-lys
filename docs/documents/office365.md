---
author: Christophe Bonvin
title:  💾 Suite Bureautique, Pack Office 365
tags:
    - niveau débutant 👍
    - inscription ✔️
    - version gratuite avec codes région (@eduhdf.fr) 📧
    

   
---

 :floppy_disk: **Pack Office 365 : Suite Bureautique** :floppy_disk:

C’est la suite bureautique de Microsoft. Une licence est offerte par la région pour chaque enseignant et chaque élève au lycée. Cette suite regroupe différents outils comme un traitement de texte (Word), un tableur (Excel), un logiciel de présentation (PowerPoint), un logiciel de messagerie (Outlook), un espace de stockage de 1To (OneDrive), et d’autres outils. Il est possible d’utiliser ces outils en ligne ou de les installer sur plusieurs appareils (5 PC ou Mac, 5 tablettes et 5 smartphones)


!!! note "Prise en main"
    - Lien vers l’outil : [https://www.office.com/](https://www.office.com/){:target="_blank" }
    
    
!!! info "Remarque"
    - Il vous faudra vos identifiants fournis par la région pour accéder au site
    - Pour votre identifiant sur office, il faudra ajouter @eduhdf.fr, exemple : mdupont@eduhdf.fr
    - Si vous n'avez pas reçu de codes, contacter le support : support365@eduhdf.fr