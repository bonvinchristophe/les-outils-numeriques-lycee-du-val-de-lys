---
author: Christophe Bonvin
title:  💾  Créer des infographies, des diaporamas, Canva
tags:
    - niveau intermédiaire 👌
    - inscription ✔️
    - version gratuite avec mail académique 📧
---


 :floppy_disk: **Canva : Créer des infographies, des diaporamas** :floppy_disk:

**Canva** est un outil est plutôt destiné aux enseignants et permet de créer des infographies pour le cours, des affiches, etc. Il propose de nombreux modèles avec une banque d’image qui est riche. 



!!! tip "Prise en main"
    - Lien vers l’outil : [https://www.canva.com/fr_fr/ ](https://www.canva.com/fr_fr/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Canva](https://ladigitale.dev/digiplay/#/v/6356c9df767a4){:target="_blank" }
    
    
!!! info "Remarque"
    -Il y a une version gratuite et une payante (Premium). S’inscrire avec son adresse académique permet d’avoir accès à davantage de ressources que la version gartuite mais sans pour autant avoir toutes les fonctionnalités de la version Premium.
