---
author: Christophe Bonvin
title:  💾  Modifier, convertir des pdf, DigiPDF
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺

   
---

 :floppy_disk: **DigiPDF : Modifier, convertir des pdf** :floppy_disk:

**DigiPDF** regroupe une série de 52 outils pour travailler sur les fichiers PDF.

Parmi les modules proposés, vous pourrez :
- convertir un fichier PDF en .odt, .docx, .odp, .pptx, .ods, .xlsx ;
- réduire la taille d'un fichier trop volumineux ;
- réorganiser, extraire ou fusionner des pages d'un fichier ;
- ajouter ou supprimer un mot de passe à un document ;
- etc

En ce qui concerne le traitement des fichiers téléversés, il y a 2 cas de figure : soit les fichiers restent du côté client (dans le navigateur), soit ils sont stockés temporairement sur le serveur durant l'exécution des tâches et supprimés par la suite (le dossier /tmp du serveur est vidé quotidiennement).

!!! note "Prise en main"
    - Lien vers l’outil : [https://digipdf.app/ ](https://digipdf.app/ ){:target="_blank" }
    - Lien vers le tutoriel : [https://ladigitale.dev/digiview/#/v/6714277c9be89](https://ladigitale.dev/digiview/#/v/6714277c9be89){:target="_blank" }