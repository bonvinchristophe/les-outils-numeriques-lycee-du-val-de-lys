---
author: Christophe Bonvin
title:  💾 Raccourcir des liens,  LSTU
tags:
    - niveau débutant 👍
    - inscription ✖️
       
---

 :floppy_disk: **LSTU : Raccourcir des liens** :floppy_disk:

Il est parfois nécessaire de communiquer un lien vers une adresse web aux élèves mais la plupart du temps, ce dernier est très long et ne dit pas grand chose sur ce qu'on va trouver. Avec **LSTU**, non seulement vous pouuvez raccourcir les liens mais également leur donner un nom plus évocateur

!!! note "Prise en main"
    - Lien vers l’outil : [https://lstu.fr/ ](https://lstu.fr/ ){:target="_blank" }
    - Exemple: [https://lstu.fr/vdl](https://lstu.fr/vdl){:target="_blank" }