---
author: Christophe Bonvin
title:  💾  Créer des infographies, des diaporamas dynamiques, Genially
tags:
    - niveau intermédiaire 👌
    - inscription ✔️
    - version gratuite avec mail académique 📧
---


 :floppy_disk: **Genially : Créer des diaporamas interactifs** :floppy_disk:

**Genially** est une application en ligne qui permet de créer des contenus interactifs, en intégrant des images, des vidéos, des objets numériques et du texte, etc. Les usages de Genially en contexte pédagogique sont nombreux et n’ont pour limite que votre créativité ! Toutefois la prise en main peut nécessiter un certain temps.
Exemples : Créer des supports de cours sous forme de dossiers interactifs et les rendre disponibles aux élèves, créer des cartes interactives en géographie, réaliser un escape game, faire une ligne du temps interactive, créer un quizz pour réviser les différents sujets abordés.
Genially propose une version gratuite qui est suffisante pour travailler. Une version premium est disponible qui permet de ranger ses créations dans des dossiers, ou d’exporter son travail en PDF, png, site web ou vidéo.


!!! note "Prise en main"
    - Lien vers l’outil : [https://genially.com/fr/](https://genially.com/fr/){:target="_blank" }
    - Lien vers le tutoriel : [Présentation](https://profpower.lelivrescolaire.fr/tutoriel-creez-une-presentation-avec-genially/){:target="_blank" } - [Vidéo sur les bases 1/2](https://ladigitale.dev/digiplay/#/v/63579dda6c533){:target="_blank" } - [Vidéo sur les bases (animations) 2/2](https://ladigitale.dev/digiplay/#/v/63579dfa83bd1){:target="_blank" }
    
    
!!! info "Remarque"
    -Il existe une version Education gratuite en s'inscrivant avec son mail académique. Elle propose plus de modèles que la version payante mais les options d'exportation sont limitées comme dans la version gratuite (pas d'exportation en pdf, ni png, ni vidéo, ni HTML)