---
author: Christophe Bonvin
title:  💾 Suite Bureautique, Libre Office 
tags:
    - niveau débutant 👍
    - utilisation facile 1️⃣
    - inscription ✖️
    

   
---

 :floppy_disk: **Libre Office : Suite Bureautique** :floppy_disk:

C’est une suite bureautique à Office 365 mais celle-ci est libre et gratuite. On y retrouve les mêmes outils exceptés l’espace de stockage.


!!! note "Prise en main"
    - Lien vers l’outil : [https://fr.libreoffice.org/download/telecharger-libreoffice/](https://fr.libreoffice.org/download/telecharger-libreoffice/){:target="_blank" }
