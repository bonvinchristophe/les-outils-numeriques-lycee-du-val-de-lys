---
author: Christophe Bonvin
title:  💾 Modifier rapidement des images en ligne, Digicard
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺

   
---

 :floppy_disk: **Digicard : Modifier rapidement des images en ligne** :floppy_disk:

**Digicard** est une application libre et gratuite de la plateforme **Ladigitale.dev** (créée par Emmanuel Zimmert). Ce module est un petit outil sans prétention pour créer des cartes virtuelles et des compositions graphiques simples à l’aide :
• de texte ;
• d’éléments graphiques (des formes, des bulles, etc.) ;
• de vos images personnelles.
Les paramètres de la composition sont automatiquement enregistrés dans le stockage local de votre navigateur.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicard/#/](https://ladigitale.dev/digicard/#/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digicard](https://ladigitale.dev/digiplay/#/v/6357d7e442e3d){:target="_blank" }
    