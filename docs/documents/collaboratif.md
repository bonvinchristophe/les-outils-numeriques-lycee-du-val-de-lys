---
author: Christophe Bonvin
title:  💾 Réaliser des documents collaboratifs simples (traitement de texte, tableur, image)
tags:
    - niveau débutant 👍
    - inscription ✖️
    - la digitale 🌺

   
---

 :floppy_disk: **Réaliser des documents collaboratifs simples (traitement de texte, tableur, image)** :floppy_disk:

#### :arrow_forward: Digidoc : Traitement de texte collaboratif
**Digidoc** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Basé sur le logiciel libre Etherpad Lite, Digidoc permet de créer des documents collaboratifs. 

_Voici des exemples d'usages pédagogiques_ : rédaction d'une histoire à plusieurs mains, développement d'un schéma narratif, débat d'idées et fil de discussion thématique, etc. Les documents peuvent être créés sans compte et par un simple clic. Il est important de récupérer immédiatement le lien vers le document (en copiant le lien dans la barre d'adresse du navigateur par exemple). L'outil propose des options de formatage pour le texte et offre la possibilité d'insérer des liens et de commenter les contributions (pour une évaluation entre pairs ou une rétroaction de l'enseignant par exemple). Les contributions des participants apparaissent en temps réel sur le document et peuvent être facilement identifiées grâce à un système de couleurs personnalisables.

!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digidoc/](https://ladigitale.dev/digidoc/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digidoc](https://ladigitale.dev/digiplay/#/v/6356c3535bafb){:target="_blank" }


#### :arrow_forward: Digicalc : Tableur collaboratif
**Digicalc** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Basé sur le logiciel libre Ethercalc, Digicalc permet de créer des feuilles de calcul collaboratives pour la mise en place d'activités et de projets pédagogiques, la collaboration entre pairs, etc.

_Exemples d'usages_ : sitographie collaborative pour répertorier des liens entre pairs, fiche de lecture, gestion de projet, comptage en classe avec plusieurs groupes, etc.

Les documents peuvent être créés sans compte et par un simple clic. Il est important de récupérer immédiatement le lien en le copiant depuis la barre d'adresse du navigateur et de le prendre en note afin de retrouver l'accès au document par la suite.


!!! note "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicalc/](https://ladigitale.dev/digicalc/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digidoc](https://ladigitale.dev/digiview/#/v/67141a5fac99f){:target="_blank" } à partir de 7min33

#### :arrow_forward: Digiboard : Un tableau blanc collaboratif
**Digiboard** est une application libre et gratuite de la plateforme Ladigitale.dev créée par Emmanuel Zimmert.

Elle permet de créer des tableaux blancs collaboratifs pour :
    • annoter ou commenter une image ;
    • faire un remue-méninge ;
    • créer des activités interactives (texte à trous, classement, glisser-déposer, etc.) ;
    • etc



!!! note "Prise en main"
    - Lien vers l’outil : [https://digiboard.app/	](https://digiboard.app/	){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiboard](https://ladigitale.dev/digiplay/#/v/6357d4fb2ae6a){:target="_blank" } 
    