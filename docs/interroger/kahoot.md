---
author: Christophe Bonvin
title:  🙋 Kahoot, questionnaires en direct
tags:
    - exercice 📄
    - niveau débutant 👍
    - inscription ✔️
---

 :raising_hand:  **Kahoot, questionnaires en direct** :raising_hand:

**Kahoot** permet de créer des questionnaires sous forme de Quizz. Cet outil s’avére très utile pour évaluer des notions de la leçon étudiée en classe.

Kahoot est une application en ligne permettant de générer des QCM interactifs avec intégration d’images et de vidéos. Ces derniers, utilisés en classe sur tablettes, téléphone portable ou ordinateur, donnent la possibilité aux élèves de s’auto-évaluer.

Il existe une bibliothèque thématique avec des questionnaires créés par d'autres personnes, toutefois la plupart sont en anglais.

!!! info "Remarque"
    Kahoot existe en version gratuite mais les fonctionnalités peuvent être limitées.

!!! danger "RGPD"
    Kahoot n'est pas conforme au RGPD, il convient de limiter les informations personnelles des élèves (demandés de mettre que le prénom ou le numéro des élèves sur la liste d'appel).


!!! note "Prise en main"
    - Lien vers l’outil : [https://kahoot.com/](https://kahoot.com/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Présentation de Kahoot](https://ladigitale.dev/digiview/#/v/671675a136ad3){:target="_blank" }

