---
author: Christophe Bonvin
title:  🙋 Créer un jeu de questions réponses, Digibuzzer
tags:
    - niveau débutant 👍
    - exercice 📄
    - inscription ✖️
    - la digitale 🌺
---

 :raising_hand:  **Digibuzzer : Créer un jeu de questions réponses** :raising_hand:

**Digibuzzer** est une application libre et gratuite de la plateforme Ladigitale.dev créée par Emmanuel Zimmert. Il propose la mise en place d'un concept simple de question / réponse autour d'un buzzer connecté. Lors de la connexion à la salle de jeu, les élèves indiquent un nom ou un pseudo et peuvent sélectionner ou téléverser un avatar (utilisation de Digiface recommandée).

L'enseignant anime ensuite la partie, sur le principe d'un Question pour un champion en présence ou à distance. Les questions sont communiquées à l'oral et l'apprenant le plus rapide peut proposer sa réponse, que l'enseignant valide ou refuse. L'attribution de points et le classement sont directement gérés dans l'application.

**Note : une salle de jeu est accessible en administration pendant une semaine (durée de la session utilisateur), uniquement sur l'appareil et le navigateur utilisé lors de la création de la salle.**


!!! note "Prise en main"
    - Lien vers l’outil : [https://digibuzzer.app/](https://digibuzzer.app/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digibuzzer](https://ladigitale.dev/digiplay/#/v/6356c621121bd){:target="_blank" }
