---
author: Christophe Bonvin
title:  🙋 Alternative à Kahoot (créer des quiz), Digistorm
tags:
    - niveau débutant 👍
    - exercice 📄
    - inscription ✖️
    - la digitale 🌺
---

 :raising_hand:  **Digistorm : Alternative à Kahoot (créer des quiz)** :raising_hand:

**Digistorm** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Elle permet de créer des quizz illustrés à réponses uniques ou multiples, en synchrone ou en asynchrone, sans limite en nombre de quizz ou d’utilisateurs. 

On peut également, avec le même compte, générer d’autres formes d’interaction participative : des sondages, des nuages de mots, des remue-méninges. Une alternative intéressante à Kahoot et autre Quizizz (qui sont limités sour leur version gratuite). 

Autre option intéressante : il est possible d’utiliser DIGISTORM sans création de compte, sur ordinateur comme sur tablette ou smartphone. C’est donc l’outil idéal pour demander à nos élèves de créer rapidement et facilement des quizz, par exemple pour un feedback après un travail de groupe ou une présentation orale...


!!! note "Prise en main"
    - Lien vers l’outil : [ https://digistorm.app/]( https://digistorm.app/){:target="_blank" }
    - Lien vers le tutoriel vidéo : 
        - [Utiliser Digistorm (sans compte version étudiant)](https://ladigitale.dev/digiplay/#/v/635698d994e23){:target="_blank" }
        - [Utiliser Digistorm (sans compte pour les enseignants))](https://ladigitale.dev/digiplay/#/v/6356990e26a56){:target="_blank" }
        - [Utiliser Digistorm (avec compte pour enseignants)](https://ladigitale.dev/digiplay/#/v/6356993467c1f){:target="_blank" }