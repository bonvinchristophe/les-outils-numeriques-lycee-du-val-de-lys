---
author: Christophe Bonvin
title:  🙋 QCM Cam, QCM en direct (alernative à Plickers)
tags:
    - exercice 📄
    - niveau débutant 👍
    - inscription ✖️
---

 :raising_hand:  **QCM Cam, QCM en direct (alernative à Plickers)** :raising_hand:

**QCM Cam** est un équivalent, français, de Plickers, développé par Sébastien Cogez, professeur de mathématiques dans l’académie de Montpellier. 

Un des gros avantage de QCMcam c’est que l’application fonctionne en local, c’est-à-dire sans Internet, si on a pris la peine de la télécharger en amont ici.
Un autre avantage c’est que rien n’est réellement stocké en ligne, donc vous pouvez indiquer les noms des élèves (contrairement à Plickers) si l’idée n’est pas de faire un sondage anonyme (pour une évaluation par exemple). On reste dans le cadre des RGPD.
Petit avantage supplémentaire sur Plickers également, sur les cartes à distribuer aux élèves [téléchargeables ici](https://qcmcam.net/ressources/planche%20codes%20aruco%204x4%20A4.pdf){:target="_blank" } les lettres A, B, C et D sont moins visibles que sur celles de Plickers ce qui permet de limiter la triche !

!!! info "Remarque"
    Plickers est une application qui propose les mêmes fonctionnalités mais la version gratuite est limitée à 5 questions. Malheureusement elle ne respecte plus les nouvelles réglementations européennes (RGPD). Si vous souhaitez utiliser Plickers, il convient d'anonymiser les élèves.

!!! note "Prise en main"
    - Lien vers l’outil : [QCMCam](https://qcmcam.net/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Chaine Vidéo QCM Cam](https://tube-numerique-educatif.apps.education.fr/w/p/1iP6p5WAzH9TWZF5DPX3YG?playlistPosition=1){:target="_blank" }

