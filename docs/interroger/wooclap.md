---
author: Christophe Bonvin
title:  🙋 Wooclap, créer des quizz (à insérer dans une présentation)
tags:
    - exercice 📄
    - niveau intermédiaire 👌
    - inscription ✔️
    - version gratuite avec mail académique 📧
---

 :raising_hand:  **Wooclap, créer des quizz** :raising_hand:

**Wooclap** est une plateforme interactive conçue pour améliorer l'engagement des élèves lors de cours. Elle permet aux utilisateurs de poser des questions, de réaliser des sondages et d'interagir en temps réel.  
Voici quelques fonctionnalités disponibles : Sondages et QCM, Questions ouvertes, Quiz intéractifs, visualisation des résultats, etc.

!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.
    
    Wooclap est un outil payant mais qui est gratuit pour les enseignants. Pour cela, l'inscription doit se faire avec votre adresse mail académique.

!!! note "Prise en main"
    - Lien vers l’outil : [Wooclap](https://www.wooclap.com/fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Débuter avec Wooclap](https://ladigitale.dev/digiview/#/v/6713f17388a09){:target="_blank" }

