---
author: Christophe Bonvin
title: 🏡 Accueil
---

# Présentation des outils numériques 

- Ce document est une compilation de ressources numériques qui pourront être utiles dans le cadre de nos missions que ce soit en classe ou en dehors de la classe. Elles sont présentées sous forme de liste qui n’est en rien exhaustive ni prescriptive.
- Pour la plupart des solutions proposées, vous trouverez un tutoriel pour l’installation et/ou l’utilisation voire des exemples d’utilisation. Si vous avez besoin d’explications supplémentaires ou le besoin de manipuler, n’hésitez à me faire signe, j’essaierai de mettre en place soit des formations en salle informatique, soit des tutoriels vidéo plus précis.
- J’ai essayé de trouver un maximum d’outils qui s’exécutent en ligne sans installation sur la machine, sans création de compte mais également suffisamment généralistes pour pouvoir être utiles pour un plus grand nombre de collègues.
- Tous les outils proposés sont gratuits (ou au moins avec une version gratuite). Ceux qui nécessitent une inscription auront un tag **"inscription"**.
- Une table des matières thématique, avec des titres cliquables, permet de naviguer plus facilement dans le document. Si vous souhaitez y voir figurer un outil qui n’est pas présent, n’hésitez pas à me le signaler.



!!! Note "Derniers ajouts :"
    
    - [Quizinière](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/exercices/quiziniere/){:target="_blank" }  
    - [Plateforme Elea](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/plateforme/elea1/){:target="_blank" }  :new:
    - [Lstu](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/documents/lstu/){:target="_blank" }
    - [QCM Cam, alternative RGPD à Plickers](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/interroger/plickers/){:target="_blank" }
    - [Wooclap](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/interroger/wooclap/){:target="_blank" }
    - [cse](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/memo/cse/){:target="_blank" }
    - [Ubisit](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/outils/usit/){:target="_blank" }
    - [Tableau en sciences](https://bonvinchristophe.forge.apps.education.fr/les-outils-numeriques-lycee-du-val-de-lys/projection/tableau/){:target="_blank" }

??? info "Remarque"

    Deux sites ressortent souvent dans ce document :

    - [La digitale](https://ladigitale.dev/){:target="_blank" } qui propose des outils gratuits et pour la plupart sans inscription.
    - [Apps Education](https://portail.apps.education.fr/){:target="_blank" } qui est un portail d’applications mis en place par l’éducation nationale (nécessite une [inscription](https://nuage03.apps.education.fr/index.php/s/LREbY3BcFt4QLej){:target="_blank" })

    
??? Danger "Mise à jour"

    **Site publié le 21/10/2024**

    Dernière mise à jour : **11/02/2025**
 
       

   





